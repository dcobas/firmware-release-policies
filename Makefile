.SUFFIXES: .texi .pdf

.texi.pdf:
	texi2pdf $^

all: firmware-release-policy.pdf blob-release-policy.pdf
all: firmware-release-mechanism.pdf

clean:
	rm -f *.aux *.cp *.fn *.ky *.log *.pdf *.pg *.toc *.tp *.vr *.dvi *.bak
