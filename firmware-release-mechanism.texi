\input texinfo   @c -*-texinfo-*- @c %**start of header
@setfilename firmware-release-mechanism
@settitle Firmware Release Mechanism
@setchapternewpage off
@c %**end of header

@titlepage
@title Firmware Release Mechanism
@subtitle 19 March 2015
@author Juan David Gonz@'alez Cobas on behalf of BE/CO/HT
@end titlepage

@headings off
@everyheading @thistitle @| @| @thispage
@node summary
@chapter Summary

This document reflects the current mechanisms by which the firmware
release policy in the BE/CO group is implemented. It is intended as
an annex to the @emph{Firmware Release Policy} document dated
February 2014.

@chapter Summary of the rules

@enumerate
@item @code{/acc/local/share/firmware}: standard binary repository
@item @code{/acc/local/share/firmware/<module-name>}:
	directory containing firmware versions for module
	@code{<module-name>}
@item @code{/acc/local/share/firmware/<module-name>} contents:
	products for the module 
	@code{<module-name>} will be found here in the form of
	@itemize @minus
		@item
		@code{<module-name>-<version-tag>.<ext>}: actual
		firmware binaries. The
		@code{<version-tag>} syntax is module-dependent (git
		SHA, dotted triple, SVN commit number, timestamp). In
		any case, 
		@code{<module-name>.<ext>} is the default name that
		drivers and utilities fetch by default for this module.
		@item
		@code{<module-name>-<version-ref>.<ext>}: symbolic links
		to actual binaries, where @code{<version-ref>} is
		one of @code{current, previous, next}. Only the
		@code{current} symlink is mandatory.
	@end itemize
@item @code{/acc/local/share/firmware/@{lab,oper@}}: deployment places for
	the test and operational environments, respectively.
@item @code{/acc/local/share/firmware/@{lab,oper@}} contents:
	only symbolic links with
	@itemize @minus
	@item name pattern: @code{<module-name>.<ext>}
	@item pointing to: @code{../<module-name>-<ver>.<ext>}, where
		@code{<ver>} stands for any one of @code{current,
		previous, next} or an explicit, module-specific version
		tag.
	@end itemize
@end enumerate

The rationale behind this set of rules is to decouple firmware
versioning from software, so that compatible upgrades can be done either
side without the need of updating the other. Moreover, this allows to
upgrade firmware by just manipulating symbolic links in a single,
centralized place.

The rules above unify several heterogeneous schemata historically
applied on an as-needed basis.  While far from perfect, the rule set
above has the virtue of unifying criteria with a @emph{convention
everybody has agreed upon}.

@chapter Elaboration of the rules

@enumerate
@item The BE/CO standard centralized repository for firmware binaries
can be found at @file{/acc/local/share/firmware}. Here is a sample of
its current contents

@smallformat
@verbatim
cs-ccr-dev3:firmware$ ls -l /acc/local/share/firmware/
total 112
drwxrwsr-x 6 dcobas   dscdev 4096 Dec 17 14:42 00-deployment
drwxr-sr-x 2 fvaga    dscdev 4096 Jul 16  2014 00-deployment-experiment
-rw-rw-r-- 1 dcobas   dscdev 2796 Nov 25 18:03 README
drwxrwsr-x 2 mcattin  dscdev 4096 Nov 27  2013 cbmia
drwxr-sr-x 2 tstana   dscdev 4096 Oct 24 09:34 conv-ttl-blo
drwxr-sr-x 2 tstana   dscdev 4096 Jan 26 11:31 conv-ttl-rs485
drwxr-sr-x 2 mcattin  dscdev 4096 Jun 30  2014 ctc
drwxr-sr-x 2 twlostow dscdev 4096 Nov 18 18:40 ctg-bst
drwxr-sr-x 2 twlostow dscdev 4096 Nov 20 12:02 ctri
drwxr-sr-x 2 twlostow dscdev 4096 Nov 20 12:04 ctrp
drwxr-sr-x 2 twlostow dscdev 4096 Feb  2 17:41 ctrv
drwxr-sr-x 2 twlostow dscdev 4096 Nov 18 18:33 ctsyn
drwxrwsr-x 2 dcobas   dscdev 4096 Nov 26  2013 cvora
drwxrwsr-x 2 mcattin  dscdev 4096 Jan 24  2014 cvorb
drwxrwsr-x 2 mcattin  dscdev 4096 Oct 10 09:49 cvorg
drwxrwsr-x 2 mcattin  dscdev 4096 Mar 13 17:18 fmc-adc
drwxrwsr-x 2 twlostow dscdev 4096 Jan  5 10:16 fmc-fine-delay
drwxr-sr-x 2 doberson dscdev 4096 Mar 13 15:38 fmc-psbtrain
drwxrwsr-x 2 fvaga    dscdev 4096 Jul  4  2014 fmc-spec
drwxr-sr-x 2 twlostow dscdev 4096 Apr 14  2014 fmc-svec
drwxrwsr-x 2 fvaga    dscdev 4096 Aug 12  2014 fmc-tdc
drwxrwsr-x 3 dcobas   dscdev 4096 Mar 18 13:39 lab
drwxr-sr-x 2 twlostow dscdev 4096 Oct 23 10:36 list
drwxr-sr-x 2 twlostow dscdev 4096 Nov 18 17:57 mtt
drwxrwsr-x 3 dcobas   dscdev 4096 Mar 11 20:23 oper
drwxrwsr-x 2 tlevens  dscdev 4096 Dec 17 18:59 spec-rf-obs-box
drwxr-sr-x 2 twlostow dscdev 4096 Mar 12  2014 wr-core
drwxr-sr-x 2 fvaga    dscdev 4096 Mar 19 09:20 wr-trig-dist
@end verbatim
@end smallformat

@item Binaries referring to a module live in its homonymous
directory. For example, the @code{ctrp} module firmware bitstreams
are found at @code{/acc/local/share/firmware/ctrp}

@smallformat
@verbatim
cs-ccr-dev3:firmware$ ls -l /acc/local/share/firmware/ctrp
total 6124
-rw-r--r-- 1 twlostow dscdev     254 Nov 20 12:04 README
-r--r--r-- 1 twlostow dscdev 3133327 Jan 12  2011 ctrp-20080408.xsvf
-r--r--r-- 1 twlostow dscdev 3133327 Jan 12  2011 ctrp-20101214.xsvf
lrwxrwxrwx 1 twlostow dscdev      18 Nov 18 18:42 ctrp-current.xsvf -> ctrp-20101214.xsvf
@end verbatim
@end smallformat

@item As can be seen from the above example, one and only one binary is
identified as the @code{current} version, marked by a symbolic
link pointing to it.
@item Other (equally unique) binaries might possibly be labelled as
@emph{previous} and @emph{next} versions. This is signalled by the same
mechanism, i.e., an unambiguously named symbolic link. Take the
@code{fmc-fine-delay} application as an example (owner, group and date
columns suppressed to fit page width):

@smallformat
@c % @verbatim
@c % cs-ccr-dev3:firmware$ ls -lrt /acc/local/share/firmware/fmc-fine-delay/
@c % total 12808
@c % -rw-r--r-- 1 twlostow dscdev 3860948 Mar 19  2014 svec-fine-delay-v2.0rc1-20140319.bin
@c % -rw-rw-r-- 1 twlostow dscdev 3860422 Mar 25  2014 svec-fine-delay-v2.0-20140331.bin
@c % -rw-r--r-- 1 twlostow dscdev 3900068 Dec  9 15:56 svec-fine-delay-v2.1-20141209.bin
@c % -rw-r--r-- 1 twlostow dscdev 1486616 Dec  9 15:56 spec-fine-delay-v2.1-20141209.bin
@c % lrwxrwxrwx 1 dcobas   dscdev      33 Jan  5 10:16 svec-fine-delay-current.bin -> svec-fine-delay-v2.1-20141209.bin
@c % lrwxrwxrwx 1 dcobas   dscdev      33 Jan  5 10:16 svec-fine-delay-previous.bin -> svec-fine-delay-v2.0-20140331.bin
@c % @end verbatim
@verbatim
cs-ccr-dev3:firmware$ ls -lrt /acc/local/share/firmware/fmc-fine-delay/
total 12808
-rw-r--r-- 3860948 svec-fine-delay-v2.0rc1-20140319.bin
-rw-rw-r-- 3860422 svec-fine-delay-v2.0-20140331.bin
-rw-r--r-- 3900068 svec-fine-delay-v2.1-20141209.bin
-rw-r--r-- 1486616 spec-fine-delay-v2.1-20141209.bin
lrwxrwxrwx      33 svec-fine-delay-current.bin -> svec-fine-delay-v2.1-20141209.bin
lrwxrwxrwx      33 svec-fine-delay-previous.bin -> svec-fine-delay-v2.0-20140331.bin
@end verbatim
@end smallformat

@item The repository contains two directories, @code{lab} and
@code{oper}. These are the standard places where firmware binaries are
taken at boot time by drivers. The @code{lab} is the search path for the
@code{lab} environment (i.e., machines having as boot server
@code{cs-ccr-felab}); the @code{oper} directory is the search path
for all operational machines in production
(i.e., machines booting from the @code{cs-ccr-{oper,oplhc,opctf}} boot servers).

@smallformat
@verbatim
cs-ccr-dev3:firmware$ ls -ld /acc/local/share/firmware/{lab,oper}
drwxrwsr-x 3 dcobas dscdev 4096 Mar 23 19:31 /acc/local/share/firmware/lab
drwxrwsr-x 3 dcobas dscdev 4096 Mar 11 20:23 /acc/local/share/firmware/oper
@end verbatim
@end smallformat

@item The contents of @code{lab} and @code{oper} is made up of symbolic links
named after the official, unversioned, default bitstream name
that drivers look for at load time.
Those symlinks point to the actual required binary, preferably through
its  @code{current,next,previous} symlink. As an example, the
@code{svec-fine-delay} application is laid out as follows:

@smallformat
@c % @verbatim
@c % cs-ccr-dev3:firmware$ ls -l /acc/local/share/firmware/{lab,oper}/svec-fine-delay*
@c % lrwxrwxrwx 1 dcobas dscdev 68 Jan  7 11:24 /acc/local/share/firmware/lab/svec-fine-delay.bin -> /acc/local/share/firmware/fmc-fine-delay/svec-fine-delay-current.bin
@c % lrwxrwxrwx 1 dcobas dscdev 68 Jan  5 10:34 /acc/local/share/firmware/oper/svec-fine-delay.bin -> /acc/local/share/firmware/fmc-fine-delay/svec-fine-delay-current.bin
@c % @end verbatim
@verbatim
cs-ccr-dev3:lab$ cd /acc/local/share/firmware/lab ; readlink svec-fine-delay.bin 
../fmc-fine-delay/svec-fine-delay-current.bin
cs-ccr-dev3:lab$ cd /acc/local/share/firmware/oper ; readlink svec-fine-delay.bin 
/acc/local/share/firmware/fmc-fine-delay/svec-fine-delay-current.bin
@end verbatim
@end smallformat

This listing means that both operational and laboratory machines
will load at boot time the
@code{current} version of the SVEC fine delay firmware.
From the listings above,
the consequence is that @code{svec-fine-delay-v2.1-20141209.bin}, the
value of the @code{current} symlink for this module, will be loaded in
both laboratory and operational environments.

Note that the @code{oper} symlink resolves to an absolute path. This is
better avoided, because, in case of relocation of the entire repository,
all the symlinks will need to be rewritten. The relative link in
@code{lab} is preferrable, not only because it makes the entire
repository automatically relocatable, but also for readability reasons.

@end enumerate

@chapter Firmware Version Feedback into CCDB

Officially accepted versions of firmware are declared in the CCDB,
under the Definitions|Hardware Types|HW Type Properties tab of the
Hardware Editor. The reference values for the firmware version can be
declared as a single value, a range, a comma-separated list or a minimum
version.

The reference values declared in the CCDB are verified at boot
time against the actually deployed firmware versions. Discrepancies are
logged and fed back into the CCDB feedback records. This is the
preferred mechanism for DIAMON to spot and signal discrepancies. In
addition, e-mail notifications are sent to the FEC responsible person,
to signal the need of an upgrade.

The reports fed back to the CCDB are created by the
@code{hwcfgchk} utility. When invoked with the @code{-s} flag, the
report can be observed in the console

@smallformat
@verbatim
cfc-2001-rpow1:~$ hwcfgchk -s
hwcfgchk tool report ...
INFO: moduleType=CTRI,slot=12,lun=0,hwcfg=OK,id=CTRI,hw_vers=0x4d066422,
							drv_vers=0x54c6ac41
INFO: moduleType=CBMIA,slot=4,lun=0,hwcfg=OK,pcb_sn=0x1100000026de8522,hw_vers=0x213
INFO: moduleType=CBMIA,slot=3,lun=1,hwcfg=OK,pcb_sn=0x5d00000026ce3422,hw_vers=0x213
INFO: moduleType=CBMIA,slot=2,lun=2,hwcfg=OK,pcb_sn=0x6900000026d39f22,hw_vers=0x213
INFO: moduleType=CBMIA,slot=1,lun=3,hwcfg=OK,pcb_sn=0x6e00000026c7c222,hw_vers=0x213
hwcfgchk tool didn't detect any mismatch.
@end verbatim
@end smallformat


@bye
